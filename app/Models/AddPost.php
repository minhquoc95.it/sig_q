<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class AddPost extends Model
{
    use HasFactory;

    protected $table = 'addpost';
    protected $primaryKey = 'id';
    public $timestamps =false;
    protected $fillable = [
        'username',
        'team',
        'phone',
        'titleen',
        'titlevn',
        'email',
        'location',
    ];
}
