<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
class AddPost extends Controller
{
    public function AddPost(Request $request)
    {
        $data = [];
        $rule = [
            "username" => "required",
            "team" => "required",
            "phone" => "required|numeric",
            "titleen" => "required",
            "titlevn" => "required",
            "email" => "required|email",
            "location" => "required"
        ];
        $message = [
            "username.required" => "Vui lòng nhập Họ và tên",
            "team.required" => "Vui lòng nhập Team",
            "phone.required" => "Vui lòng nhập Số điện thoại",
            "phone.numeric" => "Điện thoại là số, vui lòng nhập lại",
            "titleen.required" =>"Vui lòng nhập Chức danh tiếng anh",
            "titlevn.required" =>"Vui lòng nhập Chức danh tiếng viet",
            "email.required" => "Vui lòng nhập địa chỉ Emal",
            "email.email" => "Địa chỉ Email có ký tự @, vui lòng nhập lại",
            "location.required" => "Vui lòng nhập Địa chỉ"
        ];
        if($request->isMethod("post"))
        {
            $data["post"] = $request->all();
            do{
                $valid = Validator::make($request->all(), $rule,$message);
                if($valid->fails()){
                    $error = $valid->errors()->toArray();
                    $data["error"] = $error;
                    break;
                }
                $insertdata = [
                    'username' => $data['post']['username'],
                    'team' => $data['post']['team'],
                    'phone' => $data['post']['phone'],
                    'titleen' => $data['post']['titleen'],
                    'titlevn' => $data['post']['titlevn'],
                    'email' => $data['post']['email'],
                    'location' => $data['post']['location'],
                ];
                $danhsach =\App\Models\AddPost::create($insertdata);
                $data['success'] = "Đã Đăng Ký Thành Công Cho " . $data["post"]["username"] . "Với địa chỉ Email :" . $data["post"]["email"];
            }while(false);
        }
        return view("addpost.addpost",["data" => $data]);
    }
}
