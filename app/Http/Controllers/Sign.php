<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Sign extends Controller
{
    public function Sign(Request $request)
    {
        return view("sign.sign");
    }
    public function Check(Request $request)
    {
        if($request->isMethod("post")) {
            $data["post"] = $request->all();
            $email = $data["post"]["email"];
            $password = $data["post"]["password"];
            $results = DB::select('select * from user where email = ? AND password = ?', [$email,$password]);
            if(count($results)>0){
                //Login đúng về trang
                return redirect('/');
            }
            // login sai ve lai trang login
            return redirect('/sign');

        }
    }
}
